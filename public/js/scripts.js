let last;

function show(id) {
    //id of an article is its date without spaces and colons, format: (270320201153),
    //previews have the same id with suffix preview, format: (270320201153preview)
    let x = document.getElementById(id + 'preview');

    //decides whether full article is visible and toggles between
    if (x.style.display === 'none') {
        x.style.display = "block";
    } else if (x.style.display === 'block'){
        x.style.display = "none";
    }

    //last remembers previously clicked article and hides it after clicking on another
    if(last != null && last !== x){
        last.style.display = "none";
    }
    last = x;

    //if the article is clicked and full article is visible, divides panel into two columns
    let panel = document.getElementsByClassName('left-panel');
    panel[0].className = panel[0].className.replace(" col-lg-12", " col-lg-6");


    let screen = window.matchMedia("(max-width: 991px)");
    if(screen.matches) { //if the screen is for mobile device (<991px) appends full article below article
        document.getElementById(id).appendChild(document.getElementById(id + 'preview'));
        x.style.boxShadow = 'none';
        x.style.position = 'unset';
        x.style.top = 'unset';
    }
    else { //if the screen is for desktop (>991px) appends full article back to right-panel
        document.getElementById('article-panel').appendChild(document.getElementById(id + 'preview'));
        x.style.boxShadow = '4px 4px 15px 0px black';
        x.style.position = 'sticky';
        x.style.position = '-webkit-sticky';
        x.style.top = '0';
    }
}

function view() {
    document.getElementById('news-title').style.display = 'block';
    document.getElementById('bar').style.display = 'block';
}

function sort_by_date_new() {
    let main = document.getElementById('list');

    document.getElementById('up').style.color = 'gray';
    document.getElementById('down').style.color = 'white';

    [].map.call(main.children, Object).sort(function(a, b) {
        return  +a.id.match(/\d+/) - +b.id.match(/\d+/);
    }).forEach(function(elem) {
        main.appendChild(elem);
    });
}

function sort_by_date_old() {
    let main = document.getElementById('list');

    document.getElementById('down').style.color = 'gray';
    document.getElementById('up').style.color = 'white';

    [].map.call(main.children, Object).sort(function(a, b) {
        return  +b.id.match(/\d+/) - +a.id.match(/\d+/);
    }).forEach(function(elem) {
        main.appendChild(elem);
    });
}

function myFunction(description) {
    alert("Page loaded");
    var res = description.split("> ");
    document.getElementById("demo").innerHTML = res;
    console.log(res);
}



