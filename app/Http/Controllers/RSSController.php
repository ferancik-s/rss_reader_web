<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vedmant\FeedReader\Facades\FeedReader;

class RSSController extends Controller
{
    public function start(Request $request){

        $search = $request->input('search'); //takes input from search bar

        $feed = FeedReader::read($search);
        $items = $feed->get_items();

        return view('index', ['feed' => $feed, 'items' => $items]);

    }
}
