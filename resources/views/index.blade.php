<!doctype html>
<html lang="sk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>RSS Reader</title>
    <link rel="stylesheet" type="text/css" href="{{secure_asset('css/styles.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body style="background-color: #CACACA">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">RSS Reader</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <form class="form-inline my-2 my-lg-0">
            <input id="search-bar" name="search" class="form-control mr-sm-2" type="search" placeholder="{{$feed->get_link()}}" value="{{$feed->get_link()}}">
            <button id="search-btn" class="btn btn-outline-success my-2 my-sm-0" type="submit">Načítať/Obnoviť</button>
        </form>
    </div>
</nav>

<div class="page-container">
    <div id="news-title" class="news col-lg-12 bg-dark">
        <h1>
            @if($feed->get_title())
                {{$feed->get_title()}}
            @else
                Titulka novín
            @endif
        </h1>
        <p>{{$feed->get_description()}}</p>
    </div>
    <div class="row">
        <div class="left-panel col-lg-12">
            <div class="articles">
                <div class="bar bg-dark" id="bar">
                    <h1 style="float: left">Zoznam článkov:</h1>
                    <button style="float: right" onclick="sort_by_date_new()" id="down">Od najstarších &uarr;</button>
                    <button style="float: right" onclick="sort_by_date_old()" id="up">Od najnovších &darr;</button>
                </div>
                <div class="article-list" id="list">
                    @foreach($items as $item)
                        <div class="article" id="{{$item->get_date('YmdHi')}}">
                            <button onclick="show('{{$item->get_date('YmdHi')}}')">{{$item->get_title()}}</button>
                            <p>{{$item->get_description()}}</p>
                            <p>{{$item->get_date('j. m. Y,  H:i')}}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div id="article-panel" class="right-panel col-lg-6">
            @foreach($items as $item)
                <div class="full-size-article" style="display: none" id="{{$item->get_date('YmdHi')}}preview">
                    @foreach($item->get_enclosures() as $image)
                        @if($image->get_link() != null)
                            <img src="{{url($image->get_link())}}" alt="{{$item->get_title()}}">
                        @else
                            <p>Obrázok nie je k dispozícii</p>
                        @endif
                    @endforeach
                    <div class="box">
                        <h1>{{$item->get_title()}}</h1>
                        <p>{{$item->get_description()}}</p>
                        <a target="_blank" href="{{url($item->get_permalink())}}'" class="button bg-dark">Celý článok</a>
                        <p>Publikované: {{$item->get_date('j. m. Y')}} o {{$item->get_date('H:i')}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script src="{{secure_asset('js/scripts.js')}}"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

